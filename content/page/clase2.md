# Aprendizaje Automático

[Colab Supervisado](https://colab.research.google.com/drive/116_1S3aARbYA_gL5Ui2EF-9T-IWdnbXz)

[Demo Clasificación con Camara](https://teachablemachine.withgoogle.com/)

[Demo Clasificación](https://cs.stanford.edu/people/karpathy/convnetjs/demo/classify2d.html)

[Colab No Supervisado](https://colab.research.google.com/drive/1OnSSuV3KO1_tNn4XJ8KcJjkVeHBzDYuR)

[Colab No Supervisado orientado a sonido](https://colab.research.google.com/drive/1Hm8LnVczd8mgGRgktlBMgDJ1YE-tZVKc)

[Colab Ordenar imágenes en grilla con VGG](https://colab.research.google.com/drive/10tf6VPIsM3E5HknW04QUt0K1LooLya3-)

![alt text](../sklearn-chea-sheet.png)

[![alt text](../sklearn-chea-sheet2.png)](https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Scikit_Learn_Cheat_Sheet_Python.pdf)

 
