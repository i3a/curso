# Redes Neuronales Convolucionales

[Introducción](https://colab.research.google.com/drive/1nk-uERq1NJK-uQHFkETXsq_sTCBFyQaY#scrollTo=NK64nofZPWMl)

[Filtros y redes convolucionales Colab](https://colab.research.google.com/drive/18j5F5t34_m_v1xcv16RkqG3nLhNFWuUg)

[Colab Ordenar imágenes en grilla con VGG](https://colab.research.google.com/drive/10tf6VPIsM3E5HknW04QUt0K1LooLya3-)

[Colab Deepdream](https://colab.research.google.com/drive/1GrqOB_zONetCXtf2Thrk3Oe6qQymW5Fw)

[Colab Style Transfer](https://colab.research.google.com/drive/1MCT0iSOAZPXszds4IH2gzb552zBuQ-87)

[http://www.asimovinstitute.org/neural-network-zoo/](http://www.asimovinstitute.org/neural-network-zoo/)
[![](http://www.asimovinstitute.org/wp-content/uploads/2019/04/NeuralNetworkZoo20042019.png)](http://www.asimovinstitute.org/neural-network-zoo/)
