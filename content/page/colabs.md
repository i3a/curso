# Colab ZOO

## Magenta

[GAN Synth](https://colab.research.google.com/notebooks/magenta/gansynth/gansynth_demo.ipynb)

[RNN Performance](https://colab.research.google.com/notebooks/magenta/performance_rnn/performance_rnn.ipynb)

[Onset Transcription](https://colab.research.google.com/notebooks/magenta/onsets_frames_transcription/onsets_frames_transcription.ipynb)

[Latent Constraints](https://colab.research.google.com/notebooks/latent_constraints/latentconstraints.ipynb)

[Multitrack VAE](https://colab.research.google.com/notebooks/magenta/music_vae/multitrack.ipynb)

[Music VAE](https://colab.research.google.com/notebooks/magenta/music_vae/music_vae.ipynb)

[NSynth](https://colab.research.google.com/notebooks/magenta/nsynth/nsynth.ipynb)


## Style Transfer

[ST con Keras](https://colab.research.google.com/github/tensorflow/models/blob/master/research/nst_blogpost/4_Neural_Style_Transfer_with_Eager_Execution.ipynb)

[ST con scripts](https://colab.research.google.com/github/MacgyverCode/Style-Transfer-Colab/blob/master/Neural_Style_Transfer_Notebook.ipynb)

## Deepdream

[DD con TF](https://colab.research.google.com/drive/1DWcrN9WXni58MbddvlShX0wF_oeo8W_0)

[Neural Image Synth](https://colab.research.google.com/drive/1xeJAhTEwI3TNH_CJnTMq5AJuPkOs8sJ6#scrollTo=e9UmJ09FwpdI)

## GANs

[BIG GAN en español](https://colab.research.google.com/drive/1VRvVeqrWHbLzvk4ehzYzLs5q1ZBj_krp)

[Style GAN portraits](https://colab.research.google.com/github/ak9250/stylegan-art/blob/master/styleganportraits.ipynb)

[Find yourself in StyleGAN](https://colab.research.google.com/drive/139OhnW0O_3-4IrnUCXRkO9nJn38qcdsi)

## Text Generation

[GPT2](https://colab.research.google.com/drive/1VLG8e7YSEwypxU-noRNhsv5dW4NfTGce)

## WaveNet

[WaveNet Tacotron](https://colab.research.google.com/github/r9y9/Colaboratory/blob/master/Tacotron2_and_WaveNet_text_to_speech_demo.ipynb) 
