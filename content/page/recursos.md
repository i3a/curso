# Recursos

## Cursos

### Machine Learning for Artists (Gene Kogan)
[Clases](https://ml4a.github.io/classes/)

[Notebooks](https://github.com/ml4a/ml4a-guides/tree/master/notebooks)

### Creative Applications of Deep Learning with TensorFlow (Parag Mital)
[Curso en Kadenze](https://www.kadenze.com/courses/creative-applications-of-deep-learning-with-tensorflow/info)

[Github](https://github.com/pkmital/CADL)

### Wekinator (Rebecca Fiebrink)
[Curso en Kadenze](https://www.kadenze.com/courses/machine-learning-for-musicians-and-artists/info)

### Videos de Siraj Raval!
[Arte](https://www.youtube.com/watch?v=9Mxw_ilpvwA&list=PL2-dafEMk2A5Y14yGVeBwDTbx0kt93Iae)

[Música](https://www.youtube.com/watch?v=S_f2qV2_U00&list=PL2-dafEMk2A5-sn0Sgkw-4q-Lw0jiuQtu)

[Matemática para IA](https://www.youtube.com/watch?v=xRJCOz3AfYY&list=PL2-dafEMk2A7mu0bSksCGMJEmeddU_H4D)

## Artistas

[Memo Atken](http://www.memo.tv)

[Mario Klingemann](http://quasimondo.com/)

[Kyle McDonald](http://kylemcdonald.net/)

[Cristobal Valenzuela](https://cvalenzuelab.com/)

[Gene Kogan](http://genekogan.com/)


## Art y NeurIPS

[2018](http://www.aiartonline.com/)

[2018 Workshop](https://nips2018creativity.github.io/)

[2017](http://nips4creativity.com/)


## Artículos y Notas

[Science and Culture: Computers take art in new directions, challenging the meaning of “creativity”](https://www.pnas.org/content/116/11/4760)

[Formal Theory of Creativity, Fun, and Intrinsic Motivation](http://people.idsia.ch/~juergen/ieeecreative.pdf)

[The Rise of AI Art—and What It Means for Human Creativity](https://singularityhub.com/2019/06/17/the-rise-of-ai-art-and-what-it-means-for-human-creativity/)

[There’s a subreddit populated entirely by AI personifications of other subreddits](https://www.theverge.com/2019/6/6/18655212/reddit-ai-bots-gpt2-openai-text-artificial-intelligence-subreddit)

[Artificial Intelligence as a Godlike Tool for Experimentation](https://hyperallergic.com/499177/artificial-intelligence-as-a-godlike-tool-for-experimentation/)

### Leo Solaas

[Lógica sensible: observaciones sobre el acto de programar](https://medium.com/@solaas/lógica-sensible-observaciones-sobre-el-acto-de-programar-d4c15da0bfa1)

[Razones posibles para hacer arte en un mundo repleto](https://medium.com/@solaas/razones-posibles-para-hacer-arte-en-un-mundo-repleto-229fff425a79)

[Autómatas creadores: los sistemas generativos en el cruce del arte y la tecnología](https://medium.com/@solaas/aut%C3%B3matas-creadores-los-sistemas-generativos-en-el-cruce-del-arte-y-la-tecnolog%C3%ADa-f6d36dc1edd5)


## Software

### Magenta (Google Brain)
[Web](https://magenta.tensorflow.org/)

[Demos](https://magenta.tensorflow.org/demos)

### Wekinator
[Web](http://www.wekinator.org/)


### Ml5
[Web](https://ml5js.org/)


