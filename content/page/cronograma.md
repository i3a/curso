## Clase 1 (9/10 Matienzo)
    IA y Arte
    Introducción Python y Colab
## Clase 2 (16/10 Matienzo)
    Aprendizaje Automático Supervisado
    Wekinator
## Clase 3 (23/10 Matienzo)
    Aprendizaje Automático No Supervisado
    AudioStellar
## Clase 4 (30/10 Matienzo)
    Redes Neuronales Convolucionales
    Autoencoders
## Clase 5 (6/11 Matienzo)
    Redes Generativas Adversarias
## Clase 6 (13/11 Matienzo)
    Temas Varios
    Trabajo sobre obras
## Muestra (14/11 Matienzo)
    Expo y Fiesta