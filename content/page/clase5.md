# Redes Generativas

## Autoencoders

[Gitlab](https://gitlab.com/i3a/clases/blob/master/Autoencoders.ipynb)

[Colab](https://colab.research.google.com/drive/1zQCEqC-tavxGjGyIn9QxF4pMHHxMuQpl)


[Colab v2](https://colab.research.google.com/drive/1mGqHQzlq1G2mSQpo7UZUYXX3GEVYK-ZK)

### Audio

[Colab](https://colab.research.google.com/drive/1M8Jk2Dm6lhwzOkgOtV6oRliB2D_JEiva)

## GAN

### MNIST

[Gitlab](https://gitlab.com/i3a/clases/blob/master/DCGAN_MNIST.ipynb)

[Colab](https://colab.research.google.com/drive/1vouPuAYpVkc1Yo4y-ptPNkSMLGJHFRRw)

### Pokemons

[Gitlab](https://gitlab.com/i3a/clases/blob/master/DCGAN_MNIST.ipynb)

[Colab](https://colab.research.google.com/drive/11klp6y4SXF00HBiWnmVE62-i9_2TnAHD)

### StyleGAN

[Colab](https://colab.research.google.com/drive/1JD1RRbRKLHICbJ5wUMt2nFBpR-n21UYp)
