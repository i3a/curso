![](https://lh5.googleusercontent.com/mS90mycHslH2oZMqAX5ma3lKJpBxokGzEThnsJkR1_WrkneGY1lypmR1qATiaJmGr_YIAaMdbg=w768)

> (infinite wikipedia women, 2018)

# Descripción del curso

La inteligencia artificial o el aprendizaje automático ofrecen un enorme potencial para aplicaciones creativas y en este curso nos preguntaremos qué es posible hacer con ellas. Se incentivará a los participantes a explorar aplicaciones de estas técnicas para el desarrollo de sus obras o campos artísticos particulares. Veremos cómo usar sistemas de inteligencia artificial entrenados para realizar tareas creativas, generar nuevos contenidos y comportamientos, y navegaremos sobre grandes conjuntos de datos para analizar sus similitudes y diferencias.

## Perfil de los participantes:

Curiosos, auto-didactas, estudiantes y profesionales del campo informático que hayan comenzado a explorar el campo artístico.
Artistas con o sin conocimientos de programación que quieran incorporar nuevas herramientas digitales para el desarrollo de sus obras.

<div style="text-align: center">
    <a href="https://goo.gl/forms/0dhywjmk6rherRqo1" target="blank" class="btnInscripcion">Inscribite acá</a>
</div>

## Contenidos:

- Plataformas [jupyter](https://jupyter.org/) y [collab](https://colab.research.google.com/)
- Análisis y visualización de datos con [pandas](https://pandas.pydata.org/) y [matplotlib](https://matplotlib.org/)
- Introducción al aprendizaje automático con [scikit-learn](https://scikit-learn.org/stable/)
- Introducción a las redes neuronales con [keras](https://keras.io/)
- Aplicación a la creación artística

## Info

Cuando: Desde el 2 de octubre - 6 encuentros - Miércoles 19 a 22 hs

Donde: CC Matienzo, Pringles 1249 (y Av. Córdoba), Buenos Aires [(mapa)](https://goo.gl/maps/hsHSVhc2QTM2)

Costo: $4200 - 2 pagos de $2100 

### Consultas

Whatsapp: 1166213897

formacion@ccmatienzo.com.ar

[www.ccmatienzo.com.ar/formacion](www.ccmatienzo.com.ar/formacion)

Curaduría y producción ejecutiva: Julieta Agriano

## Docentes

<img class="docente" src="https://i3a.gitlab.io/curso/img/pablo_riera.jpg" />

### Pablo E. Riera

Investigador, programador, docente, músico. Doctor de la Universidad de Buenos Aires en Ciencias Físicas. Actualmente está realizando un posdoctorado en el Laboratorio de Inteligencia Artificial Aplicada de la Facultad de Ciencias Exactas y Naturales de la UBA. Realizó numerosas performances en vivo con grupos musicales, como conciertos de improvisación libre y presentaciones audiovisuales utilizando redes neuronales y electroencefalografía en tiempo real. Dirige el Proyecto de Investigación Cartografías de Espacio Tiempo y Arte Sonoro, radicado en la Universidad Nacional de Quilmes. Actualmente realiza música y performances con herramientas computacionales, matemáticas, ecuaciones diferenciales e inteligencia artificial.

<img class="docente" src="https://i3a.gitlab.io/curso/img/leandro_garber.jpg" />

### Leandro Garber

Tiene 32 años y se desarrolla como artista, profesor, programador y científico de datos en Buenos Aires. Estudió Ciencias de la Computación en la “Universidad de Buenos Aires (UBA)”, es Licenciado en Artes Electrónicas en la “Universidad de Tres de Febrero (UNTREF)” y está escribiendo su tesis sobre identificación de idioma (LID) para la Maestría en Minería de Datos y Descubrimiento del Conocimiento en la UBA. Enseña código creativo en UNTREF donde además coordina un grupo de investigación enfocado en algoritmos generativos y exploratorios de inteligencia artificial. Es científico de datos en CIIPME-CONICET donde es parte de un grupo de investigación que estudia desarrollo de lenguaje del niño.


<div style="text-align: center">
    <a href="https://goo.gl/forms/0dhywjmk6rherRqo1" target="blank" class="btnInscripcion">Inscribite acá</a>
</div>
